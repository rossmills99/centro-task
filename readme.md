# Duplicate groups task

## Shape search background

Actify's Centro product has a feature called 3DSearch.  Using 3DSearch, users are able to index their 3D CAD models within the Centro Catalog and perform searches in the Catalog based on shape-similarity or shape duplicates.  A shape duplicate is a component that is geometrically identical to another component within the Catalog.  Search results are presented as a list of objects known as CatalogParts.

## Duplicate groups

A new feature that we are interested in implementing is the idea of a "Duplicate Dashboard".  The Centro API already has the capability to provide data about "Duplicate Groups" within the Centro Catalog.  A "Duplicate Group" is a group of two or more CatalogParts which are geometric duplicates.  The "Duplicate Groups" API returns a list of all Duplicate Groups within the Catalog.

## The Duplicate groups task

The Task is to create a Web interface *using AngularJS* to present the "Duplicate Groups" data which can be found in the `duplicate_groups.json` file.

It is up to you to decide how the data should be presented however the following features would be interesting to see:

- Heirarchical view of Duplicate groups showing aggregate counts.
- Filtering and sorting of results based on Part Properties.
- Any graphical visualisation of the data.

To complete the Task you can make use of any other open source libraries (for example charting libraries, or CSS layout libraries).  
